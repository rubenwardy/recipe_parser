import { expect } from "chai";
import { parseIngredientFromLine } from "recipe_parser";

describe("IngredientParseLine", function() {
	it("parse simple", function() {
		const [ret] = parseIngredientFromLine("100g Flour");
		expect(ret.amount).eq(100);
		expect(ret.unit).eq("grams");
		expect(ret.name).eq("flour");
	});

	it("parse seperated", function() {
		const [ret] = parseIngredientFromLine("100 g Flour");
		expect(ret.amount).eq(100);
		expect(ret.unit).eq("grams");
		expect(ret.name).eq("flour");
	});

	it("parse singular", function() {
		const [ret] = parseIngredientFromLine("1 gram Flour");
		expect(ret.amount).eq(1);
		expect(ret.unit).eq("grams");
		expect(ret.name).eq("flour");
	});
});

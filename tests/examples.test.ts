import { expect } from "chai";
import { parseRecipeFromHTML } from "recipe_parser";
const fs = require("fs");

describe("Examples", function() {
	const dir = __dirname + "/examples/";
	fs.readdirSync(dir)
			.filter((file: string) => file.endsWith(".html"))
			.forEach((file: string) => {
		const name = file.slice(0, file.length - 5);
		it(name, function() {
			const html = fs.readFileSync(dir + file, "utf-8");
			const expected = JSON.parse(fs.readFileSync(dir + name + ".json", "utf-8"));
			const actual = parseRecipeFromHTML("", html);
			actual.ingredients.forEach(ingredient => {
				delete ingredient.source;
			});

			expect(actual.ingredients).eql(expected.ingredients);
		});
	});
});

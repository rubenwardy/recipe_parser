function escapeHtml(unsafe) {
	return unsafe
			.replace(/&/g, "&amp;")
			.replace(/</g, "&lt;")
			.replace(/>/g, "&gt;")
			.replace(/"/g, "&quot;")
			.replace(/'/g, "&#039;");
}

async function fetchRecipe(url, isDebug) {
	const apiUrl = new URL("parse/", window.location.href);
	apiUrl.searchParams.append("url", url);
	if (isDebug) {
		apiUrl.searchParams.append("debug", 1);
	}

	const response = await fetch(apiUrl.href);
	if (!response.ok) {
		throw Error(`${await response.text()}`);
	}
	return await response.json();
}

async function updateRecipe(url, isDebug) {
	const recipe = await fetchRecipe(url, isDebug);

	const ingredients = recipe.ingredients.map(ingredient => {
		const debug = ingredient.source ? `<br /><i>${ingredient.source}</i>` : "";

		return `<li>${ingredient.amount || ""} ${ingredient.unit || ""} <b>${ingredient.name}</b> ${debug}</li>`;
	});

	return `
		<div class="card">
			<div class="card-image">
				<figure class="image is-16by9">
					<img src="${recipe.meta.image}" alt="Image" style="object-fit: cover;">
				</figure>
			</div>
			<div class="card-content">
				<h2 class="title is-4">${recipe.meta.title}</h2>
				<p class="subtitle is-6">${recipe.meta.site_name || ""}</p>

				<div class="content">
					<h3 class="subtitle is-6 mb-0">Ingredients</h3>
					<ul>
						${ingredients.join("")}
					</ul>
				</div>
			</div>
			<footer class="card-footer">
				<a class="card-footer-item" href="${recipe.url}">
					View Original Recipe
				</a>
			</footer>
		</div>
	`;
}

document.getElementById("go").addEventListener("click", async () => {
	const info = document.getElementById("info");
	info.innerHTML = "Loading...";

	try {
		const input = document.getElementById("url");
		if (!input.value) {
			return "Expected value for text box";
		}

		const debug = document.getElementById("debug");
		console.log(debug.checked);

		info.innerHTML = await updateRecipe(input.value, debug.checked);
	} catch (err) {
		info.innerHTML = err.message;
	}
});

import * as express from "express";
import { parseRecipeFromHTML } from "recipe_parser";
const got = require("got");

const app = express();
const port = process.env.PORT || 3000;

app.use(express.static(__dirname + '/public'));

app.get("/parse/", async (req, res) => {
	try {
		if (!req.query.url) {
			res.status(400).send("Missing URL");
			return;
		}

		const url = req.query.url.toString();
		const response = await got(url);

		const recipe = parseRecipeFromHTML(url, response.body);
		if (!req.query.debug) {
			recipe.ingredients.forEach(ingredient => {
				delete ingredient.source;
			})
		}

		res.json(recipe);
	} catch (error) {
		res.status(404).send(error.toString());
		console.log(error.stack);
	}
});

app.listen(port, () => {
	console.log(`App listening at http://localhost:${port}`)
});

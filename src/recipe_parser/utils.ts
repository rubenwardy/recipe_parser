export function getWords(str: string): string[] {
	return str.split(/[\s,]+/);
}

export function replaceSubstring(str: string, start: number, length: number, replacement: string) {
	return str.substr(0, start) + replacement + str.substr(start + length);
}


/**
 * Finds positions of phrases in a line of text
 */
export class PhraseMatcher {
	beginningWith = {};

	constructor(phrases: string[]) {
		phrases.forEach(phrase => {
			const words = getWords(phrase.toLowerCase());

			let current = this.beginningWith;
			for (let i = 0; i < words.length; i++) {
				current[words[i]] = current[words[i]] || {};
				current = current[words[i]];
			}

			current["__end"] = true;
		})
	}

	findPhrasesInLine(line: string): [string, number][] {
		const words = getWords(line.toLowerCase());
		const ret: [string, number][] = []
		words.forEach((word, idx) => {
			let current = this.beginningWith[word];
			if (!current) {
				return;
			}

			let phrase = word;
			let foundPhrase: string = null;
			if (current && current["__end"]) {
				foundPhrase = phrase;
			}

			for (let j = idx + 1; j < words.length; j++) {
				const nextWord = words[j];
				const next = current[nextWord] || current[nextWord + "s"];
				if (!next) {
					break;
				}
				phrase += " " + nextWord;

				current = next;

				if (current && current["__end"]) {
					foundPhrase = phrase;
				}
			}

			if (foundPhrase) {
				ret.push([foundPhrase, idx]);
			}
		});
		return ret;
	}
}

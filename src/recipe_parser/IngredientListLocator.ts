import cheerio = require("cheerio");
import { Ingredient, parseIngredientFromLine } from "./ingredient_lines";
const assert = require('assert').strict;
import htmlToText = require('html-to-text');
import fs = require("fs");

function getLines(ele: Cheerio): string[] {
	const text = htmlToText.fromString(cheerio.html(ele), {
		wordwrap: false,
		ignoreHref: true,
		ignoreImage: true,

		format: {
			heading: () => "",
			listItem: (elem, fn, options) => {
				var h = fn(elem.children, options);
				return h + "\n";
			}
		},
	});
	return text.split("\n");
}

/**
 * Finds ingredients inside a particular element subtree.
 *
 * @param parent
 * @returns ingredients
 */
function findIngredientsInElement(parent: Cheerio): [Ingredient[], number] {
	const lines = getLines(parent);
	const lineInfo =
		lines.map(line => parseIngredientFromLine(line))
			.filter(ele => ele[0] != null);

	const ingredients = lineInfo.map(ele => ele[0]);
	const score = lineInfo.reduce((sum, x) => sum + x[1], 0)
	return [ ingredients, score ];
}

const HEADINGS = {
	"h1": true,
	"h2": true,
	"h3": true,
	"h4": true,
	"h5": true,
	"h6": true,
}


/**
 * This class is used for scoring elements in HeuristicIngredientListLocator,
 * to separate direct and indirect evidence of an ingredient list.
 *
 * The ingredients score is the points from ingredient lines - it's the most
 * important metric.
 *
 * The hints score is the points from surronding tags that suggest there may be
 * ingredients nearby, for example, a heading titled "Ingredients".
 *
 * If the ingredients score is 0, then the total score is 0.
 * The hints score is capped to at most 1.5x the ingredients score.
 */
class Score {
	ingredients: number;
	hints: number;

	constructor(ingredients: number, hints: number) {
		this.ingredients = ingredients;
		this.hints = hints;
	}

	add(other: Score) {
		this.ingredients += other.ingredients;
		this.hints += other.hints;
	}

	getScore(): number {
		if (this.ingredients <= 0) {
			return 0;
		}
		const adjusted = (this.hints > this.ingredients * 1.5) ? this.ingredients * 1.5 : this.hints;
		return this.ingredients + adjusted;
	}
}

export class IngredientListLocator {
	$: CheerioStatic;
	weights = new Map<CheerioElement, number>();

	constructor($: CheerioStatic) {
		this.$ = $;
	}

	run(start: string = "body"): CheerioElement {
		this.$("script, noscript, nav, img").remove();
		this.calculateElement(this.$(start)[0]);

		const results = [ ...this.weights.entries() ].sort((a,b) => b[1] - a[1]);
		if (results.length == 0) {
			console.warn("Unable to find ingredients");
			return null;
		}

		// Debug report
		if (false) {
			this.$("head").append(`<style>
				.recipe_parser_result {
					background: rgba(255, 0, 0, 0.3) !important;
					display: block;
				}
			</style>`);
			this.$(results[0][0]).addClass("recipe_parser_result");

			const report = cheerio.html(this.$.root());
			fs.writeFileSync("/tmp/report.html", report);

			results.slice(0, 10).forEach((result, i) => {
				console.log(`#### ${i} score ${result[1]} ####`);
				const html = cheerio.html(result[0]).slice(0, 200);
				console.log(`${html}`);
			});
		}

		return results[0][0];
	}

	private calculateElementScore(container: CheerioElement): Score {
		let hint = 0;
		if (HEADINGS[container.name]) {
			if (this.$(container).text().toLowerCase().includes("ingredient")) {
				return new Score(0, 100);
			}

			return new Score(0, 0);
		} else if (container.name == "ul") {
			hint += 10;
		}


		if (!container.children || container.children.length == 0) {
			return new Score(0, 0);
		} else if (container.name == "li" ||
				container.children.some(child => child.type == "text" && this.$(child).text().trim() != "")) {
			const [ingredients, score] = findIngredientsInElement(this.$(container))
			assert(isFinite(score) && !isNaN(score));
			return new Score(score, hint);
		} else {
			let sum = new Score(0, 0);
			let failed = 0;
			container.children.forEach(child => {
				const result = this.calculateElement(child);
				if (result.getScore() == 0) {
					failed++;
				}
				sum.add(result);
			});

			return new Score(0.5 * (sum.ingredients - failed), (sum.hints - failed) * 0.1 + hint);
		}
	}

	private calculateElement(container: CheerioElement): Score {
		const score = this.calculateElementScore(container);

		const scored = score.getScore();
		assert(isFinite(scored) && !isNaN(scored));
		if (scored > 0) {
			this.weights.set(container, scored);
		}

		this.$(container).attr("data-score", `${score.ingredients} / ${score.hints}`);
		return score;
	}
}

/**
 * Find ingredients for recipe, heuristically.
 *
 * @param $
 */
export function findIngredientsInPage($: CheerioStatic): Ingredient[] {
	const locator = new IngredientListLocator($);
	const container = locator.run();
	if (container == null) {
		return null;
	}

	const [ingredients] = findIngredientsInElement($(container));
	return ingredients;
}

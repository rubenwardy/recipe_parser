import * as cheerio from "cheerio";
import { Ingredient } from "./ingredient_lines";
import { findIngredientsInPage } from "./IngredientListLocator";

export class Recipe {
	url: string;
	meta: Map<string, string>;
	ingredients: Ingredient[];

	constructor(url: string, meta: Map<string, string>, ingredients: Ingredient[]) {
		this.url = url;
		this.meta = meta;
		this.ingredients = ingredients;
	}
}

/**
 * Get all meta
 *
 * @param $ Cheerio root
 */
function getMeta($: CheerioStatic): Map<string, string> {
	const ret = new Map<string, string>();

	$("meta[property^='og:'], meta[name^='og:']").each((i, ele) => {
		const tag = $(ele);
		const key = (tag.attr("property") || tag.attr("name")).slice(3);
		ret[key] = tag.attr("content");
	});

	if (!ret["title"]) {
		ret["title"] = $("title").first().text();
	}

	if (!ret["description"]) {
		ret["description"] = $("meta[name='description']").attr("content");
	}
	delete ret["url"];

	return ret;
}

/**
 * Parse recipe from HTML document
 *
 * @param html
 */
export function parseRecipeFromHTML(url: string, html: string): Recipe {
	const $ = cheerio.load(html);

	const ingredients = findIngredientsInPage($) || [];

	return new Recipe(url, getMeta($), ingredients);
}

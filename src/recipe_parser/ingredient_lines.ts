import { getWords, PhraseMatcher } from "./utils";

import ingredientList = require("./data/ingredients.json");
import unitList = require("./data/units.json");

const phraseMatcher = new PhraseMatcher(ingredientList);

const unitLookup = {};

unitList.forEach(unit => {
	unitLookup[unit.name] = unit.name;
	unitLookup[unit.name.slice(0, unit.name.length - 1)] = unit.name;

	unit.aliases.forEach(alias => {
		unitLookup[alias] = unit.name;
		unitLookup[alias + "."] = unit.name;
	});
});

function getUnit(str: string): string {
	return unitLookup[str.toLowerCase()];
}

declare global {
	interface Array<T> {
		mapNotNull<U>(callbackfn: (value: T, index: number, array: T[]) => U, thisArg?: any): Array<U>;
	}
}

Array.prototype.mapNotNull = function(callbackfn, thisArg) {
	const array = this;
	return array.reduce(function(result, ele, index) {
		const value = callbackfn.call(thisArg, ele, index, array);
		if (value != null) {
			result.push(value);
		}
		return result;
	}, []);
}

export class Ingredient {
	name: string;
	amount: number;
	unit: string;
	source: string;

	constructor(name: string, amount: number, unit: string, source: string) {
		this.name = name;
		this.amount = amount;
		this.unit = unit;
		this.source = source;
	}
}

function parseNumber(str: string) {
	const values = {
		"↉": 0,
		"⅒": 1/10,
		"⅑": 1/9,
		"⅛": 1/8,
		"⅐": 1/7,
		"⅙": 1/6,
		"⅕": 1/5,
		"¼": 1/4,
		"⅓": 1/3,
		"½": 1/2,
		"⅖": 2/5,
		"⅔": 2/3,
		"⅜": 3/8,
		"⅗": 3/5,
		"¾": 3/4,
		"⅘": 4/5,
		"⅝": 5/8,
		"⅚": 5/6,
		"⅞": 7/8
	};

	return values[str.trim()] || parseFloat(str);
}

function parseMeasureUnit(line: string): [number, string] {
	// Get measures, eg: 100g
	const words = getWords(line);
	const numbers = words.map(ele => [ele, parseNumber(ele)] ).filter(ele => ele[1]);
	if (numbers.length == 0) {
		return [null, null];
	}

	// Attempt to find sufixed units
	let unit: string = null;

	const suffix = numbers[0][0].toLowerCase().match(/[-0-9.]+([a-z]+)/);
	if (suffix) {
		unit = getUnit(suffix[1]);
	}

	// Find mentioned units as words
	if (!unit) {
		const units = words.mapNotNull(word => getUnit(word));
		unit = units[0] || null;
	}

	return [ numbers[0][1], unit ];
}

export function parseIngredientFromLine(line: string): [Ingredient, number] {
	// Get named ingredients, eg: floor
	const ingredients = phraseMatcher.findPhrasesInLine(line);
	if (ingredients.length == 0) {
		return [null, 0];
	}
	const name = ingredients[0][0];

	const [ amount, unit ] = parseMeasureUnit(line);

	let score = 1 + (amount ? 2 : 0) + (unit ? 4 : 0);
	if (line.length > 100) {
		score = 0;
	}

	return [ new Ingredient(name, amount, unit, line), score ];
}

# Recipe Parser

A library to parse information from recipes, and a server with a REST API.


```bash
npm install
npm run build
npm start
```

## References

* https://schollz.com/blog/ingredients/
* NYT
  * https://open.blogs.nytimes.com/2015/04/09/extracting-structured-data-from-recipes-using-conditional-random-fields/
  * https://github.com/nytimes/ingredient-phrase-tagger
* https://nlp.stanford.edu/courses/cs224n/2011/reports/rahul1-kjmiller.pdf
* https://dl.acm.org/ft_gateway.cfm?id=3324084&type=pdf&usg=AOvVaw3_sjMwfZtYORNghGqgVjrG
